package ru.tsc.marfin.tm.service;

import ru.tsc.marfin.tm.api.service.IAuthService;
import ru.tsc.marfin.tm.api.service.IUserService;
import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.exception.entity.UserNotFoundException;
import ru.tsc.marfin.tm.exception.field.LoginEmptyException;
import ru.tsc.marfin.tm.exception.field.PasswordEmptyException;
import ru.tsc.marfin.tm.exception.field.PasswordIncorrectException;
import ru.tsc.marfin.tm.exception.system.AccessDeniedException;
import ru.tsc.marfin.tm.exception.system.PermissionException;
import ru.tsc.marfin.tm.model.User;
import ru.tsc.marfin.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        final User user = Optional.ofNullable(userService.findOneByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new PasswordIncorrectException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRoles(Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = Optional.ofNullable(user.getRole())
                .orElseThrow(PermissionException::new);
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (hasRole == false) throw new PermissionException();
    }

}
