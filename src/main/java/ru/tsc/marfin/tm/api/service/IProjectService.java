package ru.tsc.marfin.tm.api.service;

import ru.tsc.marfin.tm.enumerated.Status;
import ru.tsc.marfin.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project create(String userId, String name, String description, Date dateStart, Date dateEnd);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project updateById(String userId, String id, String name, String description);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    Project changeProjectStatusById(String userId, String id, Status status);

}
