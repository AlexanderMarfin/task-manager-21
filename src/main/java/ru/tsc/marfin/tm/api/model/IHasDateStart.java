package ru.tsc.marfin.tm.api.model;

import java.util.Date;

public interface IHasDateStart {

    Date getDateStart();

    void setDateStart(Date dateStart);
}
